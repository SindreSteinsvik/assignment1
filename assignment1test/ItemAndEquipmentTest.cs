﻿using assignment1.Diablo2.Attributes;
using assignment1.Diablo2.Characters;
using assignment1.Diablo2.Exceptions;
using assignment1.Diablo2.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace assignment1test
{
    public class ItemAndEquipmentTest
    {
        Weapons testAxe = new Weapons()
        {
            ItemName = "Common axe",
            RequiredLevel = 1,
            slot = Slot.WEAPON_SLOT,
            WeaponType = WeaponType.WEAPON_AXE,
            WeaponAttributes = new WeaponAttributes() { Damage = 7, Attack_Speed = 1.1}
        };

        Armor testPlateBody = new Armor()
        {
            ItemName = "Common Plate body armor",
            RequiredLevel = 1,
            slot = Slot.BODY_SLOT,
            ArmorType = ArmorType.ARMOR_PLATE,
            ArmourAttributes = new PrimaryAttributes() { Strength = 1 }


        };

        Weapons testBow = new Weapons() {

            ItemName = "Common bow",
            RequiredLevel = 1,
            slot = Slot.WEAPON_SLOT,
            WeaponType = WeaponType.WEAPON_BOWS,
            WeaponAttributes = new WeaponAttributes() { Damage = 12, Attack_Speed = 0.8 }

        };

        Armor testClothHead = new Armor()
        {
            ItemName = "Common cloth head armor",
            RequiredLevel = 1,
            slot = Slot.HEAD_SLOT,
            ArmorType = ArmorType.ARMOR_CLOTH,
            ArmourAttributes = new PrimaryAttributes() { Intelligence = 5 }
        };


        //MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().
        [Fact]
        public void EquipWeapon_RequiredLevelTooHigh_ThrowException()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            testAxe.RequiredLevel = 2;



            //Act

            //Assert

            Assert.Throws<InvalidWeaponException>(() => test.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipArmor_RequiredLevelTooHigh_ThrowException()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            testPlateBody.RequiredLevel = 2;



            //Act

            //Assert

            Assert.Throws<InvalidArmorException>(() => test.EquipArmor(testPlateBody));
        }


        [Fact]
        public void EquipWeapon_WrongTypeOfWeapon_ThrowException()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            



            //Act

            //Assert

            Assert.Throws<InvalidWeaponException>(() => test.EquipWeapon(testBow));
        }

        [Fact]
        public void EquipArmor_WrongTypeOfArmor_ThrowException()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            



            //Act

            //Assert

            Assert.Throws<InvalidArmorException>(() => test.EquipArmor(testClothHead));
        }
        [Fact]
        public void EquipWeapon_WeaponIsSuccessesivlyEquiped_Message()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            string expected = "New weapon equipped!";

            //Act
            string actual = test.EquipWeapon(testAxe);

            //Assert

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_ArmourIsSuccessesivlyEquiped_Message()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            string expected = "New armour equipped!";

            //Act
            string actual = test.EquipArmor(testPlateBody);

            //Assert

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Dps_CalcDamageNoWeaponEquipped_ExpectedDamage() 
        {
            //Arrange
            Warrior test = new Warrior("Test");
            double expected = 1*(1+(5/100));

            //Act
            double actual = test.Dps();

            //Assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void Dps_CalcDamageWithWeaponEquipped_ExpectedDamage()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            double expected =(7* 1.1) * (1 + (5 / 100));

            //Act
            test.EquipWeapon(testAxe);
            double actual = test.Dps();

            //Assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void Dps_CalcDamageWithWeaponEquippedAndArmourEquipped_ExpectedDamage()
        {
            //Arrange
            Warrior test = new Warrior("Test");
            double expected = (7 * 1.1) * (1 + ((5+1) / 100));

            //Act
            test.EquipWeapon(testAxe);
            test.EquipArmor(testPlateBody);
            double actual = test.Dps();

            //Assert

            Assert.Equal(expected, actual);


        }






    }
}
