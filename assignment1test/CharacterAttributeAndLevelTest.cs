using assignment1.Diablo2.Attributes;
using assignment1.Diablo2.Characters;
using Newtonsoft.Json;

namespace assignment1test
{
    public class CharacterAttributeAndLevelTest
    {
        [Fact]
        public void CreatingCharacter_LevelbBeingSetTo1_ShouldReturn1()
        {
            //Arrange
            int expected = 1;

            //Act
            
            Mage mage = new Mage("Kalle");
            int actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelingUp_MakingCharacterLevel2_ShouldReturn2()
        {

            //Arrange
            int expected = 2;

            //Act
            Mage mage = new Mage("kalle");
            mage.LevelUp(1);

            int actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);    


        }
        [Fact]
        public void CreatingCharacters_CreatedWithDefaultAttributes_Mage()
        {
            //Arrange
            Mage mage = new Mage("Kalle");

            var expected = new PrimaryAttributes() { Strength = 1, Dexterity = 1, Intelligence = 8 };


            //Act

            var actuel = mage.base_Att;

            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);

            Assert.Equal(object1Json, object2Json);
            //Assert
            
        }

        [Fact]
        public void CreatingCharacters_CreatedWithDefaultAttributes_Ranger()
        {
            //Arrange
            Ranger ranger = new Ranger("Kalle");

            var expected = new PrimaryAttributes() { Strength = 1, Dexterity = 7, Intelligence = 1 };


            //Act

            var actuel = ranger.base_Att;

            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);

            //Assert
            Assert.Equal(object1Json, object2Json);
            

        }

        [Fact]
        public void CreatingCharacters_CreatedWithDefaultAttributes_Rogue()
        {
            //Arrange
            Rogue rogue = new Rogue("Kalle");

            var expected = new PrimaryAttributes() { Strength = 2, Dexterity = 6, Intelligence = 1 };


            //Acts

            var actuel = rogue.base_Att;

            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);

            //Assert
            Assert.Equal(object1Json, object2Json);


        }

        [Fact]
        public void CreatingCharacters_CreatedWithDefaultAttributes_Warrior()
        {
            //Arrange
            Warrior warrior = new Warrior("Kalle");

            var expected = new PrimaryAttributes() { Strength = 5, Dexterity = 2, Intelligence = 1 };


            //Act

            var actuel = warrior.base_Att;

            //Assert
            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);

            
            Assert.Equal(object1Json, object2Json);


        }
        [Fact]
        public void LevelingUpAttributes_LevelingUpFirstTime_Mage()
        {
            //Arrange
            Mage mage = new Mage("KAlle");
            var expected = new PrimaryAttributes() {Strength = 2, Dexterity = 2, Intelligence= 13};

            //Act

            mage.LevelUp(1);

            var actuel = mage.TotalAttributes;

            //Assert
            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);


            Assert.Equal(object1Json, object2Json);

        }

        [Fact]
        public void LevelingUpAttributes_LevelingUpFirstTime_Ranger()
        {
            //Arrange
            Ranger ranger = new Ranger("KAlle");
            var expected = new PrimaryAttributes() { Strength = 2, Dexterity = 12, Intelligence = 2 };

            //Act

            ranger.LevelUp(1);

            var actuel = ranger.TotalAttributes;

            //Assert
            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);


            Assert.Equal(object1Json, object2Json);

        }

        [Fact]
        public void LevelingUpAttributes_LevelingUpFirstTime_Rogue()
        {
            //Arrange
            Rogue rogue= new Rogue("KAlle");
            var expected = new PrimaryAttributes() { Strength = 3, Dexterity = 10, Intelligence = 2 };

            //Act

            rogue.LevelUp(1);

            var actuel = rogue.TotalAttributes;

            //Assert
            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);


            Assert.Equal(object1Json, object2Json);

        }

        [Fact]
        public void LevelingUpAttributes_LevelingUpFirstTime_Warrior()
        {
            //Arrange
            Warrior warrior = new Warrior("KAlle");
            var expected = new PrimaryAttributes() { Strength = 8, Dexterity = 4, Intelligence = 2 };

            //Act

            warrior.LevelUp(1);

            var actuel = warrior.TotalAttributes;

            //Assert
            var object1Json = JsonConvert.SerializeObject(expected);
            var object2Json = JsonConvert.SerializeObject(actuel);


            Assert.Equal(object1Json, object2Json);

        }

    }
}