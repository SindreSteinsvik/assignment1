﻿// See https://aka.ms/new-console-template for more information

using assignment1.Diablo2.Attributes;
using assignment1.Diablo2.Characters;
using assignment1.Diablo2.Items;
using static System.Net.Mime.MediaTypeNames;

//Just wrote this to get a better controll in my head (and for fun)
Console.WriteLine("Choose your Hero!");
Console.WriteLine("1: Mage");
Console.WriteLine("2: Ranger");
Console.WriteLine("3: Rogue");
Console.WriteLine("4: Warrior");
string input1 = Console.ReadLine();

int numVal = Convert.ToInt32(input1);

switch (numVal)
{
    case 1:
        Console.WriteLine(" You have chosen Mage!");
        Console.WriteLine("Choose your username: ");
        string input2 = Console.ReadLine();
        Mage mage = new Mage(input2);
        mage.charectersDisplay();
        Console.WriteLine(mage);
        Console.WriteLine("What kind of weapon do you want too equip");
        Console.WriteLine("choose between");
        Console.WriteLine("1: Staff");
        Console.WriteLine("2: Wand");
        string inputweapon = Console.ReadLine();
        int numWep = Convert.ToInt32(inputweapon);
        Item wand = new Weapons()
        {
            ItemName = "Super-Wand",
            RequiredLevel = 1,
            slot = Slot.WEAPON_SLOT,
            WeaponType = (WeaponType)WeaponType.WEAPON_WANDS,
            WeaponAttributes = new WeaponAttributes() { Damage = 3, Attack_Speed = 2 }
        };
        Item staff = new Weapons()
        {
            ItemName = "Super-Staff",
            RequiredLevel = 1,
            slot = Slot.WEAPON_SLOT,
            WeaponType = (WeaponType)WeaponType.WEAPON_STAFFS,
            WeaponAttributes = new WeaponAttributes() { Damage = 5, Attack_Speed = 1 }
        };
        if(numWep == 1)
        {
            mage.EquipWeapon((Weapons)wand);
            Console.WriteLine($"GZ you equipped {wand.ItemName}");
        }
        if(numWep == 2)
        {
            mage.EquipWeapon((Weapons)staff);
            Console.WriteLine($"GZ you equipped {staff.ItemName}");
        }
        

        
        mage.LevelUp(1);

        Console.WriteLine($"Your stats looks like this now!");

        mage.charectersDisplay();


        break;

    case 2:
        Console.WriteLine(" You have chosen Ranger!");
        Console.WriteLine("Choose your username: ");
        string input3 = Console.ReadLine();
        Ranger ranger = new Ranger(input3);
        ranger.charectersDisplay();
        Console.WriteLine(ranger);
        break;

    case 3:
        Console.WriteLine(" You have chosen Rogue!");
        Console.WriteLine("Choose your username: ");
        string input4 = Console.ReadLine();
        Rogue rogue = new Rogue(input4);
        rogue.charectersDisplay();
        Console.WriteLine(rogue);
        break;

    case 4:
        Console.WriteLine(" You have chosen Warrior!");
        Console.WriteLine("Choose your username: ");
        string input5 = Console.ReadLine();
        Warrior warrior = new Warrior(input5);
        warrior.charectersDisplay();
        Console.WriteLine(warrior);
        break;


}


