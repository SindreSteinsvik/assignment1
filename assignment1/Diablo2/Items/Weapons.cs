﻿using assignment1.Diablo2.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1.Diablo2.Items
{
    /// <summary>
    /// an enum type thats keep controll over the different WeaponType
    /// </summary>
    public enum WeaponType
    {
        WEAPON_AXE,
        WEAPON_BOWS,
        WEAPON_DAGGER,
        WEAPON_HAMMERS,
        WEAPON_STAFFS,
        WEAPON_SWORDS,
        WEAPON_WANDS,
    }




    /// <summary>
    /// Inherits from the Item class.
    /// </summary>
    public class Weapons : Item
    {
       /// <summary>
       /// making a WeaponAttributes object and getters and setters
       /// making a WeaponType object and getters and setters
       /// </summary>
        public WeaponAttributes WeaponAttributes { get; set; }
        public WeaponType WeaponType { get; set; }

    }
}
