﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1.Diablo2.Items
{
    /// <summary>
    /// an enum type thats keep controll over the different slots in the dictionary
    /// </summary>
    public enum Slot
    {
        HEAD_SLOT,
        BODY_SLOT,
        LEGS_SLOT,
        WEAPON_SLOT

    }




    /// <summary>
    /// An abstract class Item, that is parent class for Armor and Weapon
    /// </summary>
    public abstract class Item
    {

       
        /// <summary>
        /// creating getter and setters for the variable string ItemName and int RequiredLevel.
        /// </summary>
        public string ItemName { get; set; }
        public int RequiredLevel { get; set; }
        /// <summary>
        /// creating a slot object
        /// </summary>
        public Slot slot { get; set; }
        
    }
}
