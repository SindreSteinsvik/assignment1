﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assignment1.Diablo2.Attributes;

namespace assignment1.Diablo2.Items
{
    /// <summary>
    /// an enum type thats keep controll over the different ArmorTypes
    /// </summary>
    public enum ArmorType
    {
        ARMOR_CLOTH,
        ARMOR_LEATHER,
        ARMOR_MAIL,
        ARMOR_PLATE
    }
    
    /// <summary>
    /// Inherits from the Item class.
    /// </summary>
    public class Armor : Item
    {
        /// <summary>
        /// 2 differents variables used in this class.
        /// </summary>
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes ArmourAttributes { get; set; }
    }
}
