﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1.Diablo2.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string massage) : base(massage)
        {

        }

        public override string Message => "Invalid Armor Exception";
    }
}
