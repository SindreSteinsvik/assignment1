﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1.Diablo2.Attributes
{
    public class WeaponAttributes
    {
        /// <summary>
        /// 2 variables used in this method
        /// </summary>
        public int Damage { get; set; }
        public double Attack_Speed { get; set; }
    }
}
