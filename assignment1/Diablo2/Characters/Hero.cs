﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assignment1.Diablo2.Attributes;
using assignment1.Diablo2.Items;


namespace assignment1.Diablo2.Characters
{
    public abstract class Hero
    {
        /// <summary>
        /// Variables used in the Hero class
        /// </summary>
        public string Name { get; set; }
        public int Level { get; set; }

        public PrimaryAttributes base_Att { get; set; }
        public PrimaryAttributes TotalAttributes { get; set; }

        public Dictionary<Slot, Item> Equipment { get; set; }

        public double TotalDamage { get; set; }
        /// <summary>
        /// Constructer for the Hero class. Creates a new Dictionary and a new PrimaryAttributes.
        /// Set the level equal to 1.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public Hero(string name, int strength, int dexterity, int intelligence)
        {
            Level = 1;
            Name = name;
            Equipment = new Dictionary<Slot, Item>();
            base_Att = new PrimaryAttributes() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence };

            TotalStats();

        }

        /// <summary>
        /// Method for Leveling up is abstract in the Hero Class and implemented in each of the different Heros
        /// </summary>
        /// <param name="levels"></param> Takes in how many levels your hero wants to level
        public abstract void LevelUp(int levels);
        /// <summary>
        /// Calculating the different Heros Damage
        /// </summary>
        /// <returns></returns>
        public abstract double Dps();
        /// <summary>
        /// Making the different Heros equip different weapons
        /// </summary>
        /// <param name="weapon"></param> What kind of weapon is being equipped
        /// <returns></returns>
        public abstract string EquipWeapon(Weapons weapon);
        /// <summary>
        /// Making the different Heros equip different Armors
        /// </summary>
        /// <param name="armor"></param> What kind of weapon is being equipped
        /// <returns></returns>
        public abstract string EquipArmor(Armor armor);

        /// <summary>
        /// This Method Displays a Heros stats with the help of a StringBuilder
        /// </summary>
        public void charectersDisplay()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Name: {Name}");
            sb.AppendLine($"Level: {Level}");
            sb.AppendLine($"Strength: {TotalAttributes.Strength}");
            sb.AppendLine($"Dexterity: {TotalAttributes.Dexterity}");
            sb.AppendLine($"Intelligence: {TotalAttributes.Intelligence}");
            sb.AppendLine($"Damage : {TotalDamage}");

            Console.WriteLine(sb.ToString());
        }
        /// <summary>
        /// This metode is created for keeping track of the total stats of a Hero, after equipping Armor and weapon
        /// </summary>
        public void TotalStats()
        {
            TotalAttributes = CalcArmorAttributes();
            TotalDamage = CalculateWeaponDPS();
        }
        /// <summary>
        /// Calculating Weapons dps. DPS = Damage * Attack speed
        /// </summary>
        /// <returns></returns> returns DSP if the weapon is equipped, if not returns 1.
        public double CalculateWeaponDPS()
        {
            Item itemEqupped;
            bool isEqupped = Equipment.TryGetValue(Slot.WEAPON_SLOT, out itemEqupped);

            if (isEqupped)
            {
                Weapons wep = (Weapons)itemEqupped;
                return wep.WeaponAttributes.Attack_Speed * wep.WeaponAttributes.Damage;
            }
            else
            {
                return 1;
            }
        }
        /// <summary>
        /// Calculates the Heros attributes after equipping any armor. The method tests if there are any armor equipped on the head,body or leg slot
        /// </summary>
        /// <returns></returns> The method returns a PrimaryAttribute object that includes the hero base attributes + the armor attributes
        public PrimaryAttributes CalcArmorAttributes()
        {
            PrimaryAttributes armorBonus = new() { Strength = 0, Dexterity = 0, Intelligence = 0};

            bool isHead = Equipment.TryGetValue(Slot.HEAD_SLOT, out Item headItem);
            bool isBody = Equipment.TryGetValue(Slot.BODY_SLOT, out Item bodyItem);
            bool isLegs = Equipment.TryGetValue(Slot.LEGS_SLOT, out Item legItem);

            if (isHead)
            {
                Armor head = (Armor)headItem;
                armorBonus += new PrimaryAttributes()
                {
                    Strength = head.ArmourAttributes.Strength,
                    Dexterity = head.ArmourAttributes.Dexterity,
                    Intelligence = head.ArmourAttributes.Intelligence
                };
            }
            if (isBody)
            {
                Armor body = (Armor)headItem;
                armorBonus += new PrimaryAttributes()
                {
                    Strength = body.ArmourAttributes.Strength,
                    Dexterity = body.ArmourAttributes.Dexterity,
                    Intelligence = body.ArmourAttributes.Intelligence
                };
            }
            if (isLegs)
            {
                Armor leg = (Armor)headItem;
                armorBonus += new PrimaryAttributes()
                {
                    Strength = leg.ArmourAttributes.Strength,
                    Dexterity = leg.ArmourAttributes.Dexterity,
                    Intelligence = leg.ArmourAttributes.Intelligence
                };
            }

            return base_Att + armorBonus;
        }





    }
}