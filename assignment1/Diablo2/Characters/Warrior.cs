﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assignment1.Diablo2.Attributes;
using assignment1.Diablo2.Exceptions;
using assignment1.Diablo2.Items;

namespace assignment1.Diablo2.Characters
{
    public class Warrior : Hero
    {

        public Warrior(string name) : base(name, 5, 2, 1)
        {

        }

        public override double Dps()
        {
            TotalAttributes = CalcArmorAttributes();
            double weaponDmg = CalculateWeaponDPS();

            if (weaponDmg == 1)
            {
                return 1;
            }

            double characterDmg = weaponDmg * (1 + TotalAttributes.Strength / 100);


            return characterDmg;
        }


        public override string EquipArmor(Armor armor)
        {
            if (Level < armor.RequiredLevel)
            {
                throw new InvalidArmorException($" Character not not equip {armor.ArmorType}");
            }

            if (armor.ArmorType != ArmorType.ARMOR_PLATE)
            {
                throw new InvalidArmorException($" Character not not equip {armor.ArmorType}");

            }
            Equipment[armor.slot] = armor;
            return "New armour equipped!";

        
    }

        public override string EquipWeapon(Weapons weapon)
        {
            if (Level < weapon.RequiredLevel)
            {
                throw new InvalidWeaponException($"Character can not equip {weapon.WeaponType}");
            }

            if (weapon.WeaponType != WeaponType.WEAPON_AXE && weapon.WeaponType != WeaponType.WEAPON_HAMMERS && weapon.WeaponType != WeaponType.WEAPON_SWORDS )
            {
                throw new InvalidWeaponException($"Character can not equip {weapon.WeaponType}");
            }

            Equipment[weapon.slot] = weapon;
            return "New weapon equipped!";
        }

        public override void LevelUp(int levels)
        {
            PrimaryAttributes levelUp = new PrimaryAttributes() { Strength = 3 * levels, Dexterity = 2 * levels, Intelligence = 1 * levels };

            base_Att += levelUp;

            Level += 1 * levels;
            TotalStats();

        }
    }
}


