﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assignment1.Diablo2.Attributes;
using assignment1.Diablo2.Exceptions;
using assignment1.Diablo2.Items;

namespace assignment1.Diablo2.Characters
{
    public class Mage : Hero
    {
        /// <summary>
        /// Constructur for mage inherited from the Hero class
        /// </summary>
        /// <param name="name"></param>
        public Mage(string name) : base(name, 1, 1, 8)
        {

        }

        /// <summary>
        /// Overrides the LevelUp method in Hero. This is done becuase each hero gets different stats when they level up,
        /// The TotalStats method is called for updating the total stats of the hero
        /// </summary>
        /// <param name="levels"></param> the input levels just shows how many levels the mage wants to level up.
        public override void LevelUp(int levels)
        {
            PrimaryAttributes levelUp = new PrimaryAttributes() { Strength = 1 * levels, Dexterity = 1 * levels, Intelligence = 5 * levels };

            base_Att += levelUp;

            Level += 1 * levels;

            TotalStats();
            

        }
        /// <summary>
        /// Tests if required weapon level is higher the the heros level. Then checks if the weapon type is not equal to staff or wand.
        /// if one of the tests passes, a InvalideWeaponException is thrown. if not the weapon is equipped int he dictionary
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns></returns> is the weapon is equipped a massege is returned.
        /// <exception cref="InvalidWeaponException"></exception>
        public override string EquipWeapon(Weapons weapon)
        {
            if (Level < weapon.RequiredLevel)
            {
                throw new InvalidWeaponException($"Character can not equip {weapon.WeaponType}"); 
            }

            if(weapon.WeaponType != WeaponType.WEAPON_STAFFS && weapon.WeaponType != WeaponType.WEAPON_WANDS)
            {
                throw new InvalidWeaponException($"Character can not equip {weapon.WeaponType}"); 
            }

            Equipment[weapon.slot] = weapon;
            return "Weapon was Equipped!";
        }
        /// <summary>
        /// This method is pretty much the same as EquipWeapon(), just with armor. The logic is simular.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns></returns>
        /// <exception cref="InvalidArmorException"></exception>
        public override string EquipArmor(Armor armor)
        {
            if(Level < armor.RequiredLevel)
            {
                throw new InvalidArmorException($" Character not not equip {armor.ArmorType}");
            }

            if(armor.ArmorType != ArmorType.ARMOR_CLOTH)
            {
                throw new InvalidArmorException($" Character not not equip {armor.ArmorType}");

            }
            Equipment[armor.slot] = armor;
            return "Armor was Equipped!";
            
        }
        /// <summary>
        /// This method is also overrided from the Hero class. because of the different attributes each hero gets more damage from. 
        /// In this case mage gets more damage from having a high Intelligence.
        /// The method calculates the total damage and Intelligence the hero gets from armor and weapon equipped.
        /// </summary>
        /// <returns></returns> returns a double dps.
        public override double Dps()
        {
            TotalAttributes = CalcArmorAttributes();
            double weaponDmg = CalculateWeaponDPS();
            
            if(weaponDmg == 1)
            {
                return 1;
            }

            double characterDmg = weaponDmg * (1 + TotalAttributes.Intelligence / 100);
            
            
            return characterDmg;
        }
    }
}
