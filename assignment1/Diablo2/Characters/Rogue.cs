﻿using assignment1.Diablo2.Attributes;
using assignment1.Diablo2.Exceptions;
using assignment1.Diablo2.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1.Diablo2.Characters
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name, 2, 6, 1)
        {

        }

        public override double Dps()
        {
            TotalAttributes = CalcArmorAttributes();
            double weaponDmg = CalculateWeaponDPS();

            if (weaponDmg == 1)
            {
                return 1;
            }

            double characterDmg = weaponDmg * (1 + TotalAttributes.Dexterity / 100);


            return characterDmg;
        }

        public override string EquipArmor(Armor armor)
        {
            if (Level < armor.RequiredLevel)
            {
                throw new InvalidArmorException($" Character not not equip {armor.ArmorType}");
            }

            if (armor.ArmorType != ArmorType.ARMOR_MAIL)
            {
                throw new InvalidArmorException($" Character not not equip {armor.ArmorType}");

            }
            Equipment[armor.slot] = armor;
            return "New armour equipped!";

        }

        public override string EquipWeapon(Weapons weapon)
        {

            if (Level < weapon.RequiredLevel)
            {
                throw new InvalidWeaponException($"Character can not equip {weapon.WeaponType}");
            }

            if (weapon.WeaponType != WeaponType.WEAPON_DAGGER && weapon.WeaponType != WeaponType.WEAPON_SWORDS)
            {
                throw new InvalidWeaponException($"Character can not equip {weapon.WeaponType}");
            }

            Equipment[weapon.slot] = weapon;
            return "Weapon was Equipped!";
        }

        public override void LevelUp(int levels)
        {
            PrimaryAttributes levelUp = new PrimaryAttributes() { Strength = 1 * levels, Dexterity = 4 * levels, Intelligence = 1 * levels };

            base_Att += levelUp;

            Level += 1 * levels;
            TotalStats();

        }
    }
}
